# Spinnerette - The Spinnerette (2009)

13 songs • 53 minutes

Spinnerette is the debut eponymous album from Spinnerette. It was released in the UK on June 15, 2009 through Anthem Records and in the US on June 23. On June 9, Spinnerette made the entire album available for listening on their MySpace page.

--------------------------------------------------------------------------------

## Side A

| Track | Title                      |    Length |
| ----: | :------------------------- | --------: |
|     1 | Ghetto Love                |      3:32 |
|     2 | All Babes Are Wolves       |      2:28 |
|     3 | Cupid                      |      4:19 |
|     4 | Geeking                    |      4:11 |
|     5 | Baptized By Fire           |      4:35 |
|     6 | A Spectral Suspension      |      2:52 |
|     7 | Distorting A Code          |      4:04 |
|       | **Total**                  | **26:01** |

## Side B

| Track | Title                      |    Length |
| ----: | :------------------------- | --------: |
|     8 | Sex Bomb                   |      3:44 |
|     9 | Driving Song               |      4:28 |
|    10 | Rebellious Palpitations    |      2:38 |
|    11 | The Walking Dead           |      5:41 |
|    12 | Impaler                    |      2:31 |
|    13 | A Prescription For Mankind |      8:11 |
|       | **Total**                  | **27:13** |
