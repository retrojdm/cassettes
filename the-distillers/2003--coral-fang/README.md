# The Distillers - Coral Fang (2003)

11 songs • 44 minutes

--------------------------------------------------------------------------------

## Side A

| Track | Title                                |    Length |
| ----: | :----------------------------------- | --------: |
|     1 | Drain the Blood                      |      3:09 |
|     2 | Dismantle Me                         |      2:27 |
|     3 | Die on a Rope                        |      2:40 |
|     4 | The Gallow Is God                    |      4:36 |
|     5 | Coral Fang                           |      2:10 |
|     6 | The Hunger                           |      5:28 |
|     7 | Hall of Mirrors                      |      3:50 |
|       | **Total**                            | **24:20** |

## Side B

| Track | Title                                |    Length |
| ----: | :----------------------------------- | --------: |
|     8 | Beat Your Heart Out                  |      2:49 |
|     9 | Love Is Paranoid                     |      2:08 |
|    10 | For Tonight You're Only Here to Know |      3:19 |
|    11 | Deathsex                             |     12:18 |
|       | **Total**                            | **20:34** |
