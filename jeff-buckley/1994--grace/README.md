# Jeff Buckley - Grace (1994)

10 songs • 52 minutes

Grace is the only studio album by American singer-songwriter Jeff Buckley, released on August 23, 1994, by Columbia Records. The album had poor sales and received mixed reviews. However, in recent years it has dramatically risen in critical reputation.
Grace re-entered the albums chart in Australia at number 44 for the week of January 29 to February 5, 2007, 13 years after its original release date. It is currently certified 7x platinum in Australia. The album has been cited by critics and listeners as one of the greatest albums of all time.

--------------------------------------------------------------------------------

## Side A

| Track | Title                          |    Length |
| ----: | :----------------------------- | --------: |
|     1 | Mojo Pin                       |      5:43 |
|     2 | Grace                          |      5:23 |
|     3 | Last Goodbye                   |      4:36 |
|     4 | Lilac Wine                     |      4:33 |
|     5 | So Real                        |      4:44 |
|       | **Total**                      | **24:59** |

## Side B

| Track | Title                          |    Length |
| ----: | :----------------------------- | --------: |
|     6 | Hallelujah                     |      6:54 |
|     7 | Lover, You Should've Come Over |      6:45 |
|     8 | Corpus Christi Carol           |      2:57 |
|     9 | Eternal Life                   |      4:53 |
|    10 | Dream Brother                  |      5:27 |
|       | **Total**                      | **26:56** |
