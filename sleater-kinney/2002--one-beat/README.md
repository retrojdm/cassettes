# Sleater-Kinney - One Beat (2002)

12 songs • 43 minutes

One Beat is the sixth studio album by the American rock band Sleater-Kinney, released on August 20, 2002, by Kill Rock Stars. It was produced by John Goodmanson and recorded between March and April 2002 at Jackpot! Studio in Portland, Oregon. The album peaked at number 107 in the United States on the Billboard 200 and entered the Billboard Top Independent Albums at number five. One Beat was very well received by critics. Praise centered on its cathartic musical delivery and progressive politics.

--------------------------------------------------------------------------------

## Side A

| Track | Title             |    Length |
| ----: | :---------------- | --------: |
|     1 | One Beat          |      3:08 |
|     2 | Far Away          |      3:44 |
|     3 | Oh!               |      3:56 |
|     4 | The Remainder     |      3:36 |
|     5 | Light Rail Coyote |      3:08 |
|     6 | Step Aside        |      3:44 |
|       | **Total**         | **21:16** |

## Side B

| Track | Title             |    Length |
| ----: | :---------------- | --------: |
|     7 | Combat Rock       |      4:46 |
|     8 | O2                |      3:29 |
|     9 | Funeral Song      |      2:47 |
|    10 | Prisstina         |      3:30 |
|    11 | Hollywood Ending  |      3:18 |
|    12 | Sympathy          |      4:15 |
|       | **Total**         | **22:05** |
