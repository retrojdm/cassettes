# Sleater-Kinney - The Woods (2005)

10 songs • 48 minutes

The Woods is the seventh studio album by American alternative rock band Sleater-Kinney. It was released in 2005 on Sub Pop. The album was produced by Dave Fridmann and recorded from November 2004 to December 2004 at Tarbox Road Studios in Cassadaga, New York. The album received widespread critical acclaim.

--------------------------------------------------------------------------------

## Side A

| Track | Title                |    Length |
| ----: | :------------------- | --------: |
|     1 | The Fox              |      3:25 |
|     2 | Wilderness           |      3:40 |
|     3 | What's Mine Is Yours |      4:58 |
|     4 | Jumpers              |      4:24 |
|     5 | Modern Girl          |      3:01 |
|     6 | Entertain            |      4:55 |
|       | **Total**            | **24:23** |

## Side B

| Track | Title                |    Length |
| ----: | :------------------- | --------: |
|     7 | Rollercoaster        |      4:55 |
|     8 | Steep Air            |      4:04 |
|     9 | Let's Call It Love   |     11:01 |
|    10 | Night Light          |      3:40 |
|       | **Total**            | **23:40** |
