# Sleater-Kinney - All Hands on the Bad One (2000)

13 songs • 37 minutes

All Hands on the Bad One is the fifth studio album by the American rock band Sleater-Kinney, released on May 2, 2000, by Kill Rock Stars. The album was produced by John Goodmanson and recorded from December 1999 to January 2000 at Jackpot! Studio in Portland, Oregon and John & Stu's Place in Seattle, Washington. The music on the record ranges from softer melodies to fast punk rock guitar work, while the lyrics address issues such as women in rock, morality, eating disorders, feminism, music journalism, and media.

Upon release, All Hands on the Bad One reached number 177 on the US Billboard Top 200 chart and number 12 on the Heatseekers Albums chart. One song from the album, "You're No Rock n' Roll Fun", was released as a single. The album received very positive reviews from critics, who praised its consistency and the vocals by singer and guitarist Corin Tucker. All Hands on the Bad One appeared in several end-of-year lists and received a nomination for Outstanding Music Album at the 12th Annual Gay and Lesbian Alliance Against Defamation Awards.

--------------------------------------------------------------------------------

## Side A

| Track | Title                      |    Length |
| ----: | :------------------------- | --------: |
|     1 | The Ballad of a Ladyman    |      3:10 |
|     2 | Ironclad                   |      2:34 |
|     3 | All Hands on the Bad One   |      2:57 |
|     4 | Youth Decay                |      2:30 |
|     5 | You're No Rock n' Roll Fun |      2:37 |
|     6 | #1 Must Have               |      3:03 |
|     7 | The Professional           |      1:31 |
|       | **Total**                  | **18:22** |

## Side B

| Track | Title                      |    Length |
| ----: | :------------------------- | --------: |
|     8 | Was It a Lie?              |      3:15 |
|     9 | Male Model                 |      2:32 |
|    10 | Leave You Behind           |      3:27 |
|    11 | Milkshake n' Honey         |      2:52 |
|    12 | Pompeii                    |      2:43 |
|    13 | The Swimmer                |      3:46 |
|       | **Total**                  | **18:35** |
