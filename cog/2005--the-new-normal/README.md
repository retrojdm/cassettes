# Cog - The New Normal (2005)

11 songs • 73 minutes

The New Normal is the first studio album by Australian rock band Cog, released on April 12, 2005 by Difrnt Music. The album was produced in Weed, California by Sylvia Massy. The New Normal was made the album of the week on Triple J and at the J Award of 2005, the album was nominated for Australian Album of the Year.
The album peaked at number 19 on the Australian Recording Industry Association album chart.

--------------------------------------------------------------------------------

## Side A

| Track | Title                                                          |    Length |
| ----: | :------------------------------------------------------------- | --------: |
|     1 | Real Life                                                      |      5:56 |
|     2 | Anarchy OK                                                     |      6:22 |
|     3 | Silence Is Violence                                            |      7:39 |
|     4 | Resonate                                                       |      4:31 |
|     5 | The Spine                                                      |      7:10 |
|     6 | My Enemy                                                       |      3:33 |
|       | **Total**                                                      | **35:11** |

## Side B

| Track | Title                                                          |    Length |
| ----: | :------------------------------------------------------------- | --------: |
|     7 | Run                                                            |      5:08 |
|     8 | The River Song                                                 |      7:39 |
|     9 | Charades                                                       |      3:59 |
|    10 | The Doors (Now And Then My Life Feels Like It's Going Nowhere) |     10:14 |
|    11 | Naming the Elephant                                            |     10:48 |
|    11 | **Total**                                                      | **37:48** |
