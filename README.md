# Cassette J-Cards

[![Agnes Obel - Aventine (2013)](./agnes-obel/2013--aventine/thumb.png)](./agnes-obel/2013--aventine/)
[![Agnes Obel - Citizen of Glass (2016)](./agnes-obel/2016--citizen-of-glass/thumb.png)](./agnes-obel/2016--citizen-of-glass/)
[![Agnes Obel - Myopia (2020)](./agnes-obel/2020--myopia/thumb.png)](./agnes-obel/2020--myopia/)
[![Cog - The New Normal (2005)](./cog/2005--the-new-normal/thumb.png)](./cog/2005--the-new-normal/)
[![The Distillers - Coral Fang (2003)](./the-distillers/2003--coral-fang/thumb.png)](./the-distillers/2003--coral-fang/)
[![The Jezabels - She's So Hard (2009) + Dark Storm (2010)](./the-jezabels/2009--shes-so-hard-+-2010--dark-storm/thumb.png)](./the-jezabels/2009--shes-so-hard-+-2010--dark-storm/)
[![The Jezabels - Prisoner (2011)](./the-jezabels/2011--prisoner/thumb.png)](./the-jezabels/2011--prisoner/)
[![Jeff Buckley - Grace (1994)](./jeff-buckley/1994--grace/thumb.png)](./jeff-buckley/1994--grace/)
[![Nirvana - MTV Unplugged in New York (1994)](./nirvana/1994--mtv-unplugged-in-new-york/thumb.png)](./nirvana/1994--mtv-unplugged-in-new-york/)
[![Sleater-Kinney - All Hands on the Bad One (2000)](./sleater-kinney/2000--all-hands-on-the-bad-one/thumb.png)](./sleater-kinney/2000--all-hands-on-the-bad-one/)
[![Sleater-Kinney - One Beat (2002)](./sleater-kinney/2002--one-beat/thumb.png)](./sleater-kinney/2002--one-beat/)
[![Sleater-Kinney - The Woods (2005)](./sleater-kinney/2005--the-woods/thumb.png)](./sleater-kinney/2005--the-woods/)
[![Spinnerette - Spinnerette (2009)](./spinnerette/2009--spinnerette/thumb.png)](./spinnerette/2009--spinnerette/)
