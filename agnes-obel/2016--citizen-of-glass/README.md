# Agnes Obel - Citizen of Glass (2016)

10 songs • 41 minutes

Citizen of Glass is the third studio album by Danish singer-songwriter Agnes Obel, released on 21 October 2016 by PIAS Recordings. Four tracks were selected as singles: "Familiar", "Golden Green", "It's Happening Again", and "Stretch Your Eyes". Obel launched a European tour at the end of October 2016, followed by a North American tour in February 2017.

--------------------------------------------------------------------------------

## Side A

| Track | Title                |    Length |
| ----: | :------------------- | --------: |
|     1 | Stretch Your Eyes    |      5:12 |
|     2 | Familiär             |      3:56 |
|     3 | Red Virgin Soil      |      2:44 |
|     4 | It's Happening Again |      4:21 |
|     5 | Stone                |      3:57 |
|       | **Total**            | **20:10** |

## Side B

| Track | Title                |    Length |
| ----: | :------------------- | --------: |
|     6 | Trojan Horses        |      5:34 |
|     7 | Citizen of Glass     |      2:50 |
|     8 | Golden Green         |      4:00 |
|     9 | Grasshopper          |      2:39 |
|    10 | Mary                 |      5:48 |
|       | **Total**            | **20:51** |
