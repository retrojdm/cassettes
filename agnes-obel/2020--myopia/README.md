# Agnes Obel - Myopia (2020)

10 songs • 39 minutes

Myopia is the fourth studio album by Danish singer-songwriter Agnes Obel. It was released on 21 February 2020 by production studio Strange Harvest Limited. Three tracks were released as singles prior to the main release: "Island of Doom", "Broken Sleep" and "Parliament of Owls".

--------------------------------------------------------------------------------

## Side A

| Track | Title              |    Length |
| ----: | :----------------- | --------: |
|     1 | Camera's Rolling   |      4:44 | 
|     2 | Broken Sleep       |      4:56 |
|     3 | Island Of Doom     |      5:30 |
|     4 | Roscian            |      2:18 |
|     5 | Myopia             |      5:17 |
|       | **Total**          | **22:45** |

## Side B

| Track | Title              |    Length |
| ----: | :----------------- | --------: |
|     6 | Drosera            |      2:28 |
|     7 | Can't Be           |      3:27 |
|     8 | Parliament Of Owls |      2:30 |
|     9 | Promise Keeper     |      4:30 |
|    10 | Won't You Call Me  |      4:17 |
|       | **Total**          | **17:12** |
