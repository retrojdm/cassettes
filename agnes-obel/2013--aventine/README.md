# Agnes Obel - Aventine (2013)

11 songs • 41 minutes

Aventine is the second studio album by Danish singer-songwriter Agnes Obel, released on 30 September 2013 by PIAS Recordings. The album received positive reviews from music critics. It was also a commercial success, charting inside the top 40 of the charts in nine countries.

--------------------------------------------------------------------------------

## Side A

| Track | Title                  |    Length |
| ----: | :--------------------- | --------: |
|     1 | Chord Left             |      2:31 |
|     2 | Fuel to Fire           |      5:30 |
|     3 | Dorian                 |      4:49 |
|     4 | Aventine               |      4:09 |
|     5 | Run Cried the Crawling |      4:27 |
|       | **Total**              | **21:26** |

## Side B

| Track | Title                  |    Length |
| ----: | :--------------------- | --------: |
|     6 | Tokka                  |      1:31 |
|     7 | The Curse              |      5:54 |
|     8 | Pass Them By           |      3:32 |
|     9 | Words Are Dead         |      3:47 |
|    10 | Fivefold               |      2:00 |
|    11 | Smoke & Mirrors        |      3:00 |
|       | **Total**              | **19:44** |
