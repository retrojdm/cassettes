# Nirvana - MTV Unplugged in New York (1994)

14 songs • 53 minutes

MTV Unplugged in New York is a live album by American rock band Nirvana, released on November 1, 1994, by DGC Records. It features an acoustic performance recorded at Sony Music Studios in New York City on November 18, 1993, for the television series MTV Unplugged.
The show was directed by Beth McCarthy and aired on the cable television network MTV on December 16, 1993. In a break with MTV Unplugged tradition, Nirvana played mainly lesser-known material and covers of songs by the Vaselines, David Bowie, Lead Belly and Meat Puppets. Unlike prior MTV Unplugged performances, which were entirely acoustic, Nirvana used electric amplification and guitar effects during the set. They were joined by rhythm guitarist Pat Smear and cellist Lori Goldston, plus members of Meat Puppets for some songs.
MTV Unplugged was released after plans to release the performance as part of a live double-album compilation titled Verse Chorus Verse, were abandoned. It was the first Nirvana release after the suicide of singer Kurt Cobain seven months prior. It debuted at number one on the US Billboard 200 and was certified eight-times multiplatinum by the RIAA in 2020.

--------------------------------------------------------------------------------

## Side A

| Track | Title                               |    Length |
| ----: | :---------------------------------- | --------: |
|     1 | About A Girl                        |      3:37 |
|     2 | Come As You Are                     |      4:13 |
|     3 | Jesus Doesn't Want Me For A Sunbeam |      4:37 |
|     4 | The Man Who Sold The World          |      4:20 |
|     5 | Pennyroyal Tea                      |      3:40 |
|     6 | Dumb                                |      2:52 |
|     7 | Polly                               |      3:16 |
|       | **Total**                           | **26:35** |

## Side B

| Track | Title                               |    Length |
| ----: | :---------------------------------- | --------: |
|     8 | On A Plain                          |      3:44 |
|     9 | Something In The Way                |      4:01 |
|    10 | Plateau                             |      3:38 |
|    11 | Oh Me                               |      3:26 |
|    12 | Lake Of Fire                        |      2:55 |
|    13 | All Apologies                       |      4:23 |
|    14 | Where Did You Sleep Last Night      |      5:08 |
|       | **Total**                           | **27:15** |
