# The Jezabels - She's So Hard (2009)

5 songs • 25 minutes

She's So Hard is the second EP recorded and released by Australian four-piece musical group The Jezabels. It was released independently on 6 November 2009 through MGM Distribution.
The single "Easy To Love" came in at number 49 in the Triple J Hottest 100, 2010.

# The Jezabels - Dark Storm (2010)

5 songs • 23 minutes

Dark Storm is the third EP recorded and released by Australian four-piece musical group The Jezabels. It was released independently on 1 October 2010 through MGM Distribution.
The EP debuted at No. 1 on the Australian iTunes album chart in September 2010 and remained in the top 10 throughout October 2010. It received the Australian Independent Record Award for 'Best Independent Single/EP' in 2011, with The Jezabels also taking away the 'Best Independent Artist' award the same year.
The single "Mace Spray" came in at number 16 in the Triple J Hottest 100, 2010.

--------------------------------------------------------------------------------

## Side A (She's So Hard)

| Track | Title            |    Length |
| ----: | :--------------- | --------: |
|     1 | Hurt Me          |      5:46 |
|     2 | Easy To Love     |      4:49 |
|     3 | Violent Dream    |      5:00 |
|     4 | Into The Ink     |      6:25 |
|     5 | The Man Is Dead  |      3:21 |
|       | **Total**        | **25:21** |

## Side B (Dark Storm)

| Track | Title            |    Length |
| ----: | :--------------- | --------: |
|     1 | Dark Storm       |      4:52 |
|     2 | Mace Spray       |      5:07 |
|     3 | Sahara Mahala    |      5:03 |
|     4 | A Little Piece   |      4:15 |
|     5 | She's So Hard    |      4:34 |
|       | **Total**        | **23:51** |
