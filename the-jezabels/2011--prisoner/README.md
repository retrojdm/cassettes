# The Jezabels - Prisoner (2011)

13 songs • 55 minutes

Prisoner is the debut studio album by Australian indie rock band The Jezabels. It was released on 16 September 2011 through PIAS Recordings, Mom + Pop Music and Dine Alone Records. It was recorded at Sydney's Attic Studios with producer Lachlan Mitchell and mixed by Peter Katis. Prisoner was news.com.au Entertainment's album of the week during the week of its release. The album won the 2011 Australian Music Prize and was described as "a cocktail of power and elegance, rising like a force to be reckoned with. Dramatic, creative songwriting is delivered with ferocity by commanding front woman Hayley Mary. The Jezabels have firmly cemented their place in the Australian music industry and abroad."
At the J Awards of 2011, the album was nominated for Australian Album of the Year.
The album won Album of the Year at the 2012 Rolling Stone Australia Awards.

--------------------------------------------------------------------------------

## Side A

| Track | Title            |    Length |
| ----: | :--------------- | --------: |
|     1 | Prisoner         |      4:13 |
|     2 | Endless Summer   |      4:11 |
|     3 | Long Highway     |      6:02 |
|     4 | Try Colour       |      5:14 |
|     5 | Rosebud          |      4:15 |
|     6 | City Girl        |      5:24 |
|       | **Total**        | **29:19** |

## Side B

| Track | Title            |    Length |
| ----: | :--------------- | --------: |
|     7 | Nobody Nowhere   |      2:44 |
|     8 | Horsehead        |      4:29 |
|     9 | Austerlitz       |      3:04 |
|    10 | Deep Wide Ocean  |      4:44 |
|    11 | Piece Of Mind    |      4:00 |
|    12 | Reprise          |      0:56 |
|    13 | Catch Me         |      5:45 |
|       | **Total**        | **25:42** |
